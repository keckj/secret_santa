LATEXMK = latexmk
INKSCAPE = inkscape

all: paper.pdf

paper.pdf: paper.tex $(wildcard *.svg)
	$(LATEXMK) -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make paper.tex

%.eps: %.svg
	$(INKSCAPE) -z -D --file=$< --export-eps=$@

clean:
	$(LATEXMK) -CA

.PHONY: all clean
