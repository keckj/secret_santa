import math
import itertools
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt

def is_derangement(perm):
    """Returns true iff the permutation has no fixed point."""
    return all(perm[i] != i for i in range(len(perm)))


def secret_santa_derangements(N):
    """Returns all the permutationns without fixed points (derangements)."""
    people = list(range(N))
    for perm in itertools.permutations(people):
        if is_derangement(perm):
            yield list(perm)


def extract_pairings(N):
    """Counts the number of cycles of length 2 in all derangements"""
    pairings = defaultdict(lambda: 0)
    for derangement in secret_santa_derangements(N):
        np = 0
        for i,k in enumerate(derangement):
            np += int(derangement[k] == i)
        assert np%2 == 0, np
        for i in range(1, np//2+1):
            pairings[i] += 1
    return pairings


def S(n, k):
    """Compute the Stirling number of the first kind S(n, k)."""
    # Create a 2D array to store intermediate results
    stirling = [[0 for _ in range(k+1)] for _ in range(n+1)]

    # Base cases
    stirling[0][0] = 1
    for i in range(1, n+1):
        stirling[i][0] = 0

    # Fill the table using the recursive relation
    # S(n, k) = S(n-1, k-1) - (n-1) * S(n-1, k)
    for i in range(1, n+1):
        for j in range(1, min(i, k)+1):
            stirling[i][j] = stirling[i-1][j-1] - (i-1) * stirling[i-1][j]

    return stirling[n][k]


cocosd = {}
def A038205(n):
    """
    Returns the cardinal of permutations with no cycles of length 1 or 2.
    This is equivalent to the number of derangements of n where minimal cycle size is at least 3.
    """
    if n==0:
        return 1
    elif n in cocosd:
        return cocosd[n]
    else:
        s = 0
        for i in range(n, 2, -1):
            s += math.comb(n-1, i-1) * math.factorial(i-1) * A038205(n-i)
        cocosd[n] = s
        return int(s)


def A038205_bis(n):
    """Same as A038205 but with explicit formula."""
    s = 0
    for m in range(1,n+1):
        for k in range(m+1):
            f0 = math.factorial(k)*(-1)**(m-k)*math.comb(m,k)
            for i in range(n-m+1):
                f1 = abs(S(i+k,k))*math.comb(m-k, n-m-i)*2**(m-n+i)
                s += f0*f1*math.factorial(n)/math.factorial(i+k)/math.factorial(m)
    return int(s)


if __name__ == '__main__':
    e = math.exp(1)
    limPn = 1 - 1/math.sqrt(e)
    compute = True

    Ns, Es = [], []
    for N in range(2, 13+8*(1-compute)):
        Dn = int(round(math.factorial(N)/e))

        nderangements_without_cycles_of_length2 = A038205(N)
        An = A038205_bis(N)
        assert nderangements_without_cycles_of_length2 == An, 'ooopsy'

        Pn = 1.0 - An/Dn

        En = abs(Pn-limPn)

        if compute:
            pairings = extract_pairings(N)
            assert Dn-An == pairings[1], 'oopsy'
            pp = '  '.join(f'|P{i}|={pairings[i]/Dn:4.6f}' for i in range(1, N//2+1))
        else:
            pp = ''

        print(f"n={N}  Dn={Dn} An={An} Pn={Pn:4.6f} En={En:2.1e} {pp}")

        Ns.append(N)
        Es.append(En)


    # Fit a line to n and log_En
    log_Es = np.log(Es)
    coefficients = np.polyfit(Ns, log_Es, 1)
    polynomial = np.poly1d(coefficients)
    fitted_log_Es = polynomial(Ns)
    fitted_En = np.exp(fitted_log_Es)

    min_n, max_n = int(min(Ns)), int(max(Ns))

    plt.figure()
    plt.plot(Ns, Es, marker='o', zorder=10, label='$E_n$')
    plt.plot(Ns, fitted_En, '--', zorder=0, c='r', label='$E_n^*$')
    plt.yscale('log')
    plt.xticks(range(min_n, max_n + 1))
    plt.xlabel('n')
    plt.ylabel('En')
    plt.legend()
    plt.title('Convergence towards the asymptotic formula of $P_n$')
    plt.grid(True)

    a, b = coefficients
    latex_text = r'$E_n \simeq e^{%.2fn %+.2f}$' % (a, b)
    plt.text(10, 1e-5, latex_text, fontsize=12, rotation=-35)

    plt.savefig('convergence_plot.svg', format='svg', dpi=300)
