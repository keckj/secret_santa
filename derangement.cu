__constant__ unsigned long long factorials[21] =  { 
    1ull, 1ull, 2ull, 6ull, 24ull, 120ull, 720ull, 5040ull, 
    40320ull, 362880ull, 3628800ull, 39916800ull, 479001600ull, 
    6227020800ull, 87178291200ull, 1307674368000ull, 20922789888000ull, 
    355687428096000ull, 6402373705728000ull, 121645100408832000ull, 
    2432902008176640000ull
};

template <int n>
__device__ bool factorial_decomposition(unsigned long long number, char* permutation) {
    bool used[n];
    bool is_derangement = true;

    #pragma unroll
    for (int i = 0; i < n; i++) {
        used[i] = false;
        permutation[i] = 0;
    }
    
    #pragma unroll
    for (int i = 0; i < n; i++) {
        long long factor = number / factorials[n - 1 - i];
        number %= factorials[n - 1 - i];
        
        #pragma unroll
        for (int j = 0; j < n; j++) {
            factor -= !used[j] || factor==-1ll;
            permutation[i] += (factor==-1ll)*j;
            used[j] |= (factor == -1ll);
        }
        is_derangement &= (permutation[i]!=i);
    }
    return is_derangement;
}


template <int n>
__global__ void test_factorial_decomposition(
        unsigned long long permutation_offset,
        unsigned long long per_thread_permutations,
        unsigned long long* volatile global_counters) {

    static_assert(n>0 && n<=20, "n! with n>20 does not fit into 64bits.");
    static_assert(sizeof(unsigned long long)==8, "sizeof(uint64) != 8");
    
    static constexpr unsigned int ncounters = 2+n/2;
    
    const unsigned int tid = threadIdx.x;
    assert(blockDim.x >= ncounters);
    
    __shared__ unsigned long long shared_counters[ncounters];
    if (tid < ncounters) {
        shared_counters[tid] = 0ull;
    }
    __syncthreads();
   
    const unsigned long long threadId = blockIdx.x*(size_t)(blockDim.x) + tid;
    const unsigned long long permutation_start = permutation_offset + threadId*per_thread_permutations;
    const unsigned long long permutation_end = min(
            permutation_start + per_thread_permutations,
            factorials[n]);
    
    unsigned long long local_counters[ncounters] = {0ull};
    char permutation[n];

    for (unsigned long long k=permutation_start; k<permutation_end; ++k) {
        bool is_derangement = factorial_decomposition<n>(k, permutation);

        local_counters[0] += 1;
        local_counters[1] += is_derangement;

        if (!is_derangement) { 
            continue;
        }

        int ncouples = 0;
        {
            #pragma unroll
            for (int i = 0; i < n; i++) {
                ncouples += i == permutation[permutation[i]];
            }
            assert(ncouples%2==0);
            ncouples /= 2;
        }
            
        for (int i = 0; i < ncouples; i++) {
            local_counters[2+i] += 1;
        }
    }
    
    for (int i = 0; i < ncounters; i++) {
        atomicAdd(shared_counters+i, local_counters[i]);
    }
    __syncthreads();
    
    if (tid < ncounters) {
        atomicAdd(global_counters+tid, shared_counters[tid]);
    }
}
