# Secret Santa Secrets

## Overview
This repository contains the source code and LaTeX files for the paper titled "Probabilistic Analysis of Two-Person Loops in Secret Santa Assignments". The paper investigates the probabilities of various loop structures occurring in a Secret Santa setup, with a focus on two-person loops and more complex configurations.

## Repository Structure
- `sss.py` - Contains all the Python source code for probability calculations.
- `paper.tex` - Contains the LaTeX source files for the paper.

## Prerequisites
- Python 3
- LaTeX environment (e.g., TeX Live) with the `latexmk` and `make` utilities.
- Inkscape to generate the pdf from the svg plots

## Running the Code
To run the simulations and generate the probability data execute the following command:
```
python3 sss.py
```
