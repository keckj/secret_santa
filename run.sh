#!/bin/bash
#OAR -n derangement_17
#OAR -l nodes=7/gpu=4,walltime=04:00:00
#OAR -p gpumodel='A100' or gpumodel='V100'
#OAR --project hysop
#host IN ('bigfoot1', 'bigfoot2', 'bigfoot3', 'bigfoot4', 'bigfoot5', 'bigfoot6', 'bigfoot8')

set -e
source /applis/environments/cuda_env.sh 12.1

cd /home/keckj/secret_santa

ngpus=28
echo "Running with ${ngpus} gpus."

hostnames='bigfoot1,bigfoot1,bigfoot1,bigfoot1,bigfoot1,bigfoot2,bigfoot2,bigfoot2,bigfoot2,bigfoot3,bigfoot3,bigfoot3,bigfoot3,bigfoot4,bigfoot4,bigfoot4,bigfoot4,bigfoot5,bigfoot5,bigfoot5,bigfoot5,bigfoot6,bigfoot6,bigfoot6,bigfoot6,bigfoot8,bigfoot8,bigfoot8,bigfoot8'

mpirun -host "${hostnames}" -machinefile "${OAR_NODE_FILE}" -mca plm_rsh_agent "oarsh" -np "$((ngpus+1))" python3 -u derangement.py 17
