import os, math, time, socket, argparse

from mpi4py import MPI

import numpy as np
import cupy as cp

np.set_printoptions(threshold=np.inf, linewidth=np.inf)

script_dir = os.path.dirname(os.path.realpath(__file__))

work_chunk_size = 16*1024*1024*1024  # 16B permutations per task

main_comm = MPI.COMM_WORLD.Dup()
"""Main communicator"""
main_rank = main_comm.Get_rank()
"""Rank of the current process in main communicator"""
main_size = main_comm.Get_size()
"""Number of mpi process in main communicator"""

worker_comm = main_comm.Split(color=main_rank>0, key=main_rank)
"""Communicator between shared memory local master ranks"""

shm_comm = worker_comm.Split_type(MPI.COMM_TYPE_SHARED, main_rank)
"""Shared memory communicator"""
shm_rank = shm_comm.Get_rank()
"""Shared memory process id in shm_comm (ie. NUMA node id)"""
shm_size = shm_comm.Get_size()
"""Shared memory process count in shm_comm (ie. NUMA nodes count)"""

intershm_comm = worker_comm.Split(color=int(shm_rank==0), key=main_rank)
"""Communicator between shared memory local master ranks"""


def format_time(seconds):
    hours, remaining_seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(remaining_seconds, 60)
    return f"{int(hours):02d}:{int(minutes):02d}:{int(seconds):02d}"


def load_derangement_kernel(n):
    """
    Loads the 'derangement.cu::test_factorial_decomposition<n>' kernel function.
    """

    cuda_source = os.path.join(script_dir, 'derangement.cu')
    with open(cuda_source, 'r') as f:
        cuda_code = f.read()

    kernel_name = f'test_factorial_decomposition<{n}>'

    module = cp.RawModule(code=cuda_code, backend='nvrtc',
            name_expressions=(kernel_name,), options=('-std=c++17', '--ptxas-options', '-O3'))

    return module.get_function(kernel_name)


def worker_process(n):
    """
    Function for worker processes to request, perform tasks, and send back counters to master.
    """
    host_name = socket.gethostname()
    print(f'[{main_rank}] Running on host {host_name}.')

    device = cp.cuda.Device(shm_rank)
    device.use()

    device_props = cp.cuda.runtime.getDeviceProperties(device.id)
    
    device_name = device_props["name"].decode("utf-8")
    device_pci_bus_id = device.pci_bus_id
    device_identifier = f'{host_name}:{device_name}:{device_pci_bus_id}'
        
    print(f'[{main_rank}] Running GPU worker {main_rank} for n={n} on device {device_identifier}.')

    device_identifiers = shm_comm.gather(device_identifier, root=0)
    if shm_rank == 0:
        one_device_per_rank = (len(device_identifiers) == len(set(device_identifiers)))
        if not one_device_per_rank:
            print('The same device has been affected to multiple ranks on a given compute node.')
            print(device_identifiers)
            main_comm.Abort()

    kernel = load_derangement_kernel(n)
    max_block_size = 2**int(math.floor(math.log2(kernel.max_threads_per_block)))
    
    max_couples = n//2
    gpu_counters = cp.zeros(2+max_couples, dtype=cp.uint64)
    
    # we compute 1 billion permutations per kernel call
    K = 1024*1024//max_block_size  # per thread permutations
    B = max_block_size             # block main_size
    G = 1024                       # grid main_size
    P = (work_chunk_size + G*B*K - 1) // (G*B*K)
   
    assert work_chunk_size % G*B*K == 0, (work_chunk_size, G*B*K)
    assert P*G*B*K == work_chunk_size, (work_chunk_size, G*B*K)
        
    main_comm.send(None, dest=0, tag=0)
   
    while True:
        task_id = main_comm.recv(source=0, tag=0)
        if task_id == -1:
            break
        
        start_event = cp.cuda.Event()
        start_event.record()

        for p in range(P):
            permutation_offset = task_id * work_chunk_size + p*G*B*K

            grid_size = (G,)
            block_size = (B,)
            kernel_args = (cp.uint64(permutation_offset), cp.uint64(K,), gpu_counters)

            kernel(grid_size, block_size, kernel_args)

        end_event = cp.cuda.Event()
        end_event.record()

        end_event.synchronize()
        cpu_counters = gpu_counters.get()
        
        time_elapsed_s = cp.cuda.get_elapsed_time(start_event, end_event) / 1000.0
        permutations_per_s = cpu_counters[0] / time_elapsed_s
        print(f'  [{main_rank}] task_id={task_id}  {permutations_per_s/1000_000_000:2.2f}Tp/s')
        
        main_comm.send(cpu_counters, dest=0, tag=0)
        gpu_counters.fill(0)


    print(f'[{main_rank}] All done.')
    
    main_comm.Barrier()


def master_process(n):
    """
    Function for the master process to distribute tasks and track performance.
    """
    e = math.exp(1)

    Fn = math.factorial(n)
    Dn = int(round(Fn/e))
    
    max_couples = n//2
    counters = np.zeros(2+max_couples, dtype=np.uint64)

    task_count = (Fn + work_chunk_size - 1) // work_chunk_size
    nworkers = main_size - 1

    if nworkers == 0:
        print('No workers available, aborting.')
        main_comm.Abort()

    print(f'Running master process, {task_count} tasks to distribute among {nworkers} GPU worker processes on {intershm_comm.Get_size()} compute nodes.')

    task_id = 0
    completed_tasks = 0

    start_time = time.time()
    last_time = start_time

    status = MPI.Status()
    
    while completed_tasks < task_count:
        data = main_comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
        source = status.Get_source()

        if task_id < task_count:
            main_comm.send(task_id, dest=source, tag=0)
            task_id += 1
        
        if data is not None:
            completed_tasks += 1
            counters += data
        
        current_time = time.time() 
        if current_time - last_time > 60:
            runtime = current_time - start_time 
            tops = completed_tasks * work_chunk_size / 1_000_000_000
            total_tops_per_second = tops / runtime

            remaining_tasks = (task_count - completed_tasks) * work_chunk_size / 1_000_000_000
            estimated_time_end = int(math.ceil(remaining_tasks / total_tops_per_second))
            print(f"Tasks completed {completed_tasks}/{task_count}, {total_tops_per_second:2.2f}Tp/s, estimated time to end: {format_time(estimated_time_end)}, counters {counters}")

            last_time = current_time

    for i in range(1, main_size):
        main_comm.send(-1, dest=i, tag=0)

    main_comm.Barrier()

    runtime = current_time - start_time 
    tops = Fn / 1_000_000_000
    total_tops_per_second = tops / runtime
    
    time.sleep(2)

    print(f"All tasks completed, global permutations/sec: {total_tops_per_second:2.2f}Tp/s")
    print(f"Final results for n={n}:")
    print("  ", np.asarray(counters))
    print("  ", list(map(float, np.asarray(counters) / Dn)))
    
    assert counters[0] == Fn, (counters[0], Fn)
    assert counters[1] == Dn, (counters[1], Dn)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate all derangement of n and count 2-cycles.')
    parser.add_argument('n', type=int, help='Number of people n')

    args = parser.parse_args()
    n = args.n

    if main_rank == 0:
        master_process(n)
    else:
        worker_process(n)
