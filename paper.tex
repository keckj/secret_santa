\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath, amssymb}
\usepackage{graphicx, float, hyperref}

\title{Secret Santa Secrets}
\author{Jean-Baptiste Keck, Margaux Leroy}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}
Consider a group of $n\geq2$ people participating in a Secret Santa. Each person is randomly assigned to another person to whom they give a gift. We want to compute the probability of a two-person loop occuring. This term describes a situation where at least two participants are assigned to give gifts to each other.

\section{The Mathematics of Gift Exchange}

For $n$ people, a Secret Santa draw can be represented as a permutation $\sigma$ on the set $\{1, 2, \dots, n\}$, where $\sigma(i) \neq i$ for all $i$. 
In combinatorial mathematics, such permutation with no fixed points is called a derangement.
The total number of draws, noted $!n$, can be computed as 
$\left\lfloor \frac{n!}{e} \right\rceil 
= \left\lfloor \frac{n!}{e} + \frac{1}{2} \right\rfloor$
where e is Euler's number.

\vspace{0.5cm}
\noindent Now we need to count the number of permutations with no cycles of length 1 or 2, which is the same as the number of derangements with minimal cycle size 3.
It can be easily shown that:
$$
A(n) =  \displaystyle\sum_{m=1}^n
    \left[
    \sum_{k=0}^m 
        (-1)^{m-k}k!\binom{k}{m}
    \left[
    \sum_{i=0}^{n-m} 
        \left|S(i+k,k)\right|
        \dfrac{n!}{(i+k)!m!}
        \binom{n-m-i}{m-k} 2^{m-n+i}
    \right]
    \right]
$$

\vspace{0.1cm}
\noindent where $S(i+k, k)$ refers to the Stirling numbers of the first kind. 

\vspace{0.5cm}
\noindent Thus, the probability $P(n)$ of a two-person loop occuring can be computed as:

$$
P(n) = 1-\dfrac{A(n)}{!n}
= 1-\dfrac{A(n)}{\left\lfloor \frac{n!}{e} \right\rceil}
$$


\section{To Infinity and Beyond}

As this formula is not really practical, you will find here asymptotic formulas for a high number of participants $n$.
By performing asymptotic analysis for $n\rightarrow\infty$ on $A(n)$ we obtain directly 
$A(n)\sim \frac{n!}{e\sqrt{e}}$ and $P(n)\sim 1 - \frac{1}{\sqrt{e}} \simeq 0.393469$.

\section{Elve's Numerical Experiments}
Our workforce of skilled elves was tasked with identifying all derangements for reasonable values of $n$. 
Specifically, they focused on pinpointing the permutations where at least one cycle of length 2 was present.
Elves found the same results as our formulas by bruteforcing all derangements. 
We note $E_n = \left|P_n - P_n^*\right|$ the absolute error between the exact probability and its asymptotic value
$1 - \frac{1}{\sqrt{e}}$.


\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
\( n \) & \( D_n \) & \( A_n \) & \( P_n \) & \( E_n \) \\
\hline
2 & 1 & 0 & 1.000000 & \(6.1 \times 10^{-1}\) \\
3 & 2 & 2 & 0.000000 & \(3.9 \times 10^{-1}\) \\
4 & 9 & 6 & 0.333333 & \(6.0 \times 10^{-2}\) \\
5 & 44 & 24 & 0.454545 & \(6.1 \times 10^{-2}\) \\
6 & 265 & 160 & 0.396226 & \(2.8 \times 10^{-3}\) \\
7 & 1854 & 1140 & 0.385113 & \(8.4 \times 10^{-3}\) \\
8 & 14833 & 8988 & 0.394054 & \(5.8 \times 10^{-4}\) \\
9 & 133496 & 80864 & 0.394259 & \(7.9 \times 10^{-4}\) \\
10 & 1334961 & 809856 & 0.393349 & \(1.2 \times 10^{-4}\) \\
11 & 14684570 & 8907480 & 0.393412 & \(5.7 \times 10^{-5}\) \\
12 & 176214841 & 106877320 & 0.393483 & \(1.4 \times 10^{-5}\) \\
13 & 2290792932 & 1389428832 & 0.393473 & \(3.2 \times 10^{-6}\) \\
14 & 32071101049 & 19452141696 & 0.393468 & \(1.1 \times 10^{-6}\) \\
15 & 481066515734 & 291781655984 & 0.393469 & \(1.3 \times 10^{-7}\) \\
\hline
\end{tabular}
\caption{Hand computed values of \( n \), \( D_n \), \( A_n \), \( P_n \), and \( E_n \)}
\label{table:data}
\end{table}

\vspace{0.3cm}
\noindent The convergence in absolute error is exponential as seen in the following plot:
\vspace{-0.5cm}
\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{convergence\_plot.pdf}
  \caption{Elves were proud to show that that $n=\infty \Leftrightarrow n\geq8$.}
\end{figure}

\section{Conclusion and perspectives}
In conclusion, our investigation into the phenomenon of two-person loops in a Secret Santa arrangement reveals a remarkable and counterintuitive aspect of random assignments. As the group size $n$ increases, the probability of encountering at least one such loop converges exponentially towards a steady value. Our analysis, grounded in probabilistic theory and combinatorial calculations, demonstrates that irrespective of the group size beyond $n=6$, the likelihood of a two-person loop stabilizes around 39\%.

\vspace{0.3cm}
\noindent Looking ahead, there are intriguing avenues for further research stemming from our initial findings. Numerical simulations, conducted by brute-forcing all possible derangements up to $n=15$ (which encompasses around 481 billions derangements), indicate that the occurrence of multiple two-person loops follows a similar pattern of convergence. Specifically, the probability of two distinct sets of people being mutually assigned to each other also appears to stabilize rapidly, converging to approximately 9.0\%. Similarly, for scenarios involving three santa couples, our preliminary data suggests a convergence around 1.4\%.

\section{References}
The authors would like to thank the Online Encyclopedia of Integer Sequences (OEIS) for providing useful references about the following sequences:
\begin{itemize}
    \item \href{https://oeis.org/A000166}{A000166}: Number of permutations of $n$ elements with no fixed points.
    \item \href{https://oeis.org/A038205}{A038205}: Number of derangements of $n$ where the minimal cycle size $\geq3$.
\end{itemize}
The code used to perform the numerical experiments found in this paper is available at \href{https://gitlab.com/keckj/secret_santa}{https://gitlab.com/keckj/secret\_santa}.

\begin{center}
\includegraphics[width=0.55\textwidth]{ss.png}
\end{center}

\end{document}
